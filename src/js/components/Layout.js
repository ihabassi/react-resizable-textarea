import React from "react";

import ResizableTextArea from "./ResizableTextArea";

class Layout extends React.Component {
    render() {
      return (
          <div className="wrapper">
              <ResizableTextArea maxLength={120} />
          </div>
      );
  }
}

export default Layout;
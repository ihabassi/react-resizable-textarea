import React from "react";

class ResizableTextArea extends React.Component {
    constructor(props) {
        super(props);
        const defaultProps = {
            maxLength: 100,
            rows: 3,
            fontSize: 13,
            lineHeight: 15
        };
        // init props, if a prop doesn't exist use default
        this._maxLength = this._isPropExist('maxLength') ? props.maxLength : defaultProps.maxLength;
        this._defaultRows = this._isPropExist('defaultRows') ? props.defaultRows : defaultProps.rows;
        this._fontSize = this._isPropExist('fontSize') ? props.fontSize : defaultProps.fontSize;
        this._lineHeight = this._isPropExist('lineHeight') ? props.lineHeight : defaultProps.lineHeight;
    }

    _isPropExist(key) {
        return (typeof this.props[key] !== 'undefined' && this.props[key] !== null);
    }

    _isClassExist(element, className) {
        return element.classList.contains(className);
    }

    _lineNumberstGet(textarea) {
        const cols = (textarea.cols);
        // get the lines with the line breaks.
        let lineBreaks = textarea.value.split("\n");
        let lineWrapCount = 0;
        // calculate the lines with word wrap
        for (let i = 0; i < lineBreaks.length; i += 1) {
            // check if each line with line break is more than one line because of word wrap, and sum the result.
            lineWrapCount += Math.floor(lineBreaks[i].length / cols);
        }
        // the total lines number in the textarea
        return lineWrapCount + lineBreaks.length;
    }

    _heightResize(textarea, linesNum) {
        if (linesNum > this._defaultRows - 1) {
            textarea.rows = linesNum + 1;
            textarea.style.maxHeight = (linesNum + 1) * this._lineHeight + 'px';
        } else {
            textarea.rows = this._defaultRows;
            textarea.style.maxHeight = this._defaultRows * this._lineHeight + 'px';
        }
    }
    // change the line status by toggling classes
    _statusChange(textarea, linesNum) {
        if (textarea.value.length >= this._maxLength) {
            textarea.classList.remove('line-active');
            textarea.classList.add('line-disabled');
        } else {
            if (!this._isClassExist(textarea, 'line-active')) {
              textarea.classList.remove('line-disabled');
              textarea.classList.add('line-active');
            }
        }
    }

    _textChange(e) {
        // get the textarea lines number
        let linesNum = this._lineNumberstGet(e.target);
        // resize the height of the textarea based on the lines number and the line height
        this._heightResize(e.target, linesNum);
        // change the line color when disabled
        this._statusChange(e.target, linesNum);
    }

    render() {
        let height = (this._defaultRows) * this._lineHeight + 'px';
      return (
          <textarea placeholder={'test'} onChange={this._textChange.bind(this)} className="line-active custom-textarea" style={{lineHeight: this._lineHeight + 'px', fontSize: this._fontSize + 'px', maxHeight: height + 'px'}} maxLength={this._maxLength} rows={this._defaultRows} />
      );
    }
}

export default ResizableTextArea;